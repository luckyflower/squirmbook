note: there is at least 10 available sections of 3 bytes that store rx, tx, and device_type
in our implementation it starts at 0x1 and ends at 0x1F (that implements 10 devices)
for example you can use input device 1 without extra firmware

note: 0x0 HAS TO BE BOUND TO POWER
note: the a7 register will contain the ecall code
a0 contains the current function to call
and a1 contains the argument
here is the ecall codes of our bios:
	0x9 (power)
	0xA (video)
	0xB (audio)
	0xC (input)
	0xD (storage)
	0xE (network)
	0xF (clock)
here is the functions of each ecall:
	power:
		0x0 REBOOT
		0x1 POWEROFF
		0x2 LOW POWER (changes speed to 3 instructions per second)
		0x3 REGULAR (changes speed to normal)
	video:
		0x0 RESET (reboot screen)
		0x1 SOFT RESET (clear screen)
		0x2 SPRAY (sets cursor to every position in the a1,a2,a3,a4 area)
		0x3 SET CURSOR X 
		0x4 SET CURSOR Y
		0x5 PAINT (sets the pixel color in truecolor a1=r,a2=g,a3=b)
		0x6 ORIGIN (sets the cursor's x and y positions to 0)
		0x7 GET COLOR (gets the current pixel color in truecolor a1,a2,a3)
		0x8 GET RESOLUTION (a0 = width, a1 = height)
		0x9 GET CURSOR (a0 = x,a1 = y)
		0xA SET TARGET SCREEN (a1 = device)
	audio:
		0x0 RESET (reboot specific audio device)
		0x1 SPEAK (device will play a1=frequency a2=duration a3=device)
		0x2 SHUSH (device will be disabled until told to speak a1=device)
		0x3 LISTEN (gets the current byte of a microphone a1=device)
		0x4 COVER YOUR EARS (microphone will be disbled until told to listen a1=device)
	input:
		0x0 SET INPUT (explodes device specified on a1)
		0x1 GET INPUT (a1 = device to get info from and it will return to a0)
	storage:
		0x0 RESET
		0x1 SET CHAR (a1 = device,a2 = char,a3 = block,a4 = byte)
		0x2 GET CHAR (a1 = device,a2 = char,a3 = block,a4 = byte)
	network:
		0x0 RESET
		0x1 listen (a1 = device,a2 = port)
		0x2 connect (a1 = device,a2 = address,a3 = port)
	clock:
		0x0 RESET (time will be set to midnight january first 0 AD)
		0x1 SET YEAR (a1 = year)
		0x2 SET SECOND (a1 = second)
