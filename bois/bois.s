# squirmbook
# Copyright (C) 2022 Aaron Brown
# 
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
# if you have to contact me. contact at luckyenterr@protonmail.com
# this will set power to regular
li t0,1
sb t0,0(zero)
#

.equ dfun_rx,0x0
.equ dfun_tx,0x1
.equ dfun_device_type,0x2
.equ dtype_null,0x0
.equ dtype_video,0x1
.equ dtype_audio,0x2
.equ dtype_input,0x3
.equ dtype_storage,0x4
.equ dtype_network,0x5
.equ dtype_clock,0x6
.macro qdev register, fun
	# qdev function locates the device
	mv t1,\register
	li t0,\fun
	mul t2,t0,t1
	addi t2,t1,0x1 # 0x1 is the start of device index
	mv t0,t2
.endm
.macro daffodil chk,addr
	li t0,\chk
	beq t0,a0,\addr
	j end_\addr
	\addr:
.endm
.macro lbl name
	\name:
.endm
.macro end name
	ret
	lbl end_\name
.endm
call decide
ret
# this file is under construction :D
decide:
	li t0,0x9
	beq a7,t0,power
	li t0,0xA
	beq a7,t0,video
	li t0,0xB
	beq a7,t0,audio
	li t0,0xC
	beq a7,t0,input
	li t0,0xD
	beq a7,t0,storage
	li t0,0xE
	beq a7,t0,network
	li t0,0xF
	beq a7,t0,clock
	li a6,0x1 # if a6 is one, that means that the call failed
	ret

power:
	daffodil 0x0,pwr_reboot
		j 0000000
	end pwr_reboot
	daffodil 0x1,pwr_off
		li t0,0
		sb t0,0(zero)
	end pwr_off
	daffodil 0x2,pwr_low
		li t0,2
		sb t0,0(zero)
	end pwr_low
	daffodil 0x3,pwr_regular
		li t0,1
		sb t0,0(zero)
	end pwr_regular
vsync:
	lb t6,0(t4)
	beq t6,zero,vsync
	ret
video:
	li t2,0x1F
	li t3,0x1
	li t5,0x0
	screen_fetch:
		add t5,t5,t3
		bge t5,t2,fail
		qdev t5,dfun_device_type
		li t4,dtype_video
		beq t0,t4,video_func
		j screen_fetch
	video_func:
	qdev t5,dfun_tx
	mv t3,t0
	qdev t5,dfun_rx
	mv t4,t0
	.macro vidrun char
		li t0,\char
		sb t0,0(t3)
		call vsync
	.endm
	.macro vidrun_r reg
		mv t0,\reg
		sb t0,0(t3)
		call vsync
	.endm
	daffodil 0x0,vid_reset
		vidrun 0x0
	end vid_reset
	daffodil 0x1,vid_soft
		vidrun 0x2
	end vid_soft
	daffodil 0x2,vid_spray
		vidrun 0x3 # draw instruction
		vidrun 0x1 # spray 
		vidrun_r a1
		vidrun_r a2
		vidrun_r a3
		vidrun_r a4
		vidrun 0x0 # end draw instruction
	end vid_spray
	daffodil 0x3,vid_setx
		vidrun 0x3 # draw instruction
		vidrun 0x2 # set cursor position
		vidrun 0x1 # x
		vidrun_r a1
		vidrun 0x0 # end draw instruction
	end vid_setx
	daffodil 0x4,vid_sety
		vidrun 0x3 # draw instruction
		vidrun 0x2 # set cursor position
		vidrun 0x2 # y
		vidrun_r a1
		vidrun 0x0 # end draw instruction
	end vid_sety
	daffodil 0x5,vid_paint
		vidrun 0x3 # draw instruction
		vidrun 0x3 # paint
		vidrun_r a1
		vidrun_r a2
		vidrun_r a3
		vidrun 0x0 # end draw instruction
	end vid_paint
	daffodil 0x6,vid_origin
		vidrun 0x3 # draw instruction
		vidrun 0x2 # set cursor position
		vidrun 0x1 # x
		vidrun 0x1 # 0
		vidrun 0x0 # end draw instruction
		vidrun 0x3 # draw instruction
		vidrun 0x2 # set cursor position
		vidrun 0x2 # y
		vidrun 0x1 # 0
		vidrun 0x0 # end draw instruction
	end vid_origin
	daffodil 0x7,vid_get_color
		vidrun 0x1 # start info stream
		vidrun 0x7 # color
		vidrun 0x1 # red
		mv a1,t6
		vidrun 0x2 # green
		mv a2,t6
		vidrun 0x3 # blue
		mv a3,t6
		vidrun 0x0 # end info stream
	end vid_get_color
	daffodil 0x8,vid_get_resolution
		vidrun 0x1 # start info stream
		vidrun 0x2 # resolution
		vidrun 0x1 # x
		mv a1,t6
		vidrun 0x2 # y
		mv a2,t6
		vidrun 0x0 # end info stream
	end vig_get_resolution
	daffodil 0x9,vid_get_cursor
		vidrun 0x1 # start info stream
		vidrun 0x3 # cursor
		vidrun 0x1 # x
		mv a1,t6
		vidrun 0x2 # y
		mv a2,t6
		vidrun 0x0 # end info stream
	end vid_get_cursor
	daffodil 0xA,vid_set_target_screen
		vidrun 0x3 # draw instruction
		vidrun 0x4 # screen
		vidrun_r a1 # a1 is the device
		vidrun 0x0 # end draw instruction
	end vid_set_target_screen
	ret
input:
	li t0,0x0
	beq a0,t0,SET_INPUT
	li t0,0x1
	beq a0,t0,SET_OUTPUT
	ret
SET_INPUT:
	qdev a1,dfun_tx #locates transmitter of device
	sb a2,0(t0)
	ret
GET_INPUT:
	qdev a1,dfun_rx #locates transmitter of device
	lb a0,0(t0)
	ret
audio:
	daffodil 0x0,reset_audio
		qdev a1,dfun_tx
		li t3,0x0
		sb t3,0(t0)
	end reset_audio
	daffodil 0x1,audio_speak
		qdev a3,dfun_tx 
		sb a1,0(t0)
		# toilet sleep here
	end audio_speak
	daffodil 0x2,SHUSH
		# it does what the
		# close door button does
		# in a elevator
	end SHUSH
	daffodil 0x3,audio_listen
		qdev a1,dfun_rx
		sb a0,0(t0)
	end audio_listen
	daffodil 0x4,cover_your_ears
		qdev a1,dfun_tx
		li t1,0x0
		sb t1,0(t0)
	ret
# toilet storage
# toilet network
# toilet clock
fail: ret
